package com.examen.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen.entities.Articulo;

@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Long> {

	@Modifying
	@Query("Update Articulo a set a.cantidad = :cantidad where a.id = :idArticulo ")
	void actualizarCantidad(Long idArticulo, Integer cantidad);

}
