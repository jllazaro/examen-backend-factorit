package com.examen.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen.entities.Carrito;

@Repository
public interface CarritoRepository extends JpaRepository<Carrito, Long> {

	@Query("SELECT c FROM Carrito c join Usuario u ON u.carritoActual = c.id WHERE c.fechaCreacion < :hoy ")
	List<Carrito> carritosParaEliminar(LocalDateTime hoy);

}
