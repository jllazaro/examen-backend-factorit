package com.examen.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.examen.entities.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByUsuarioAndPassword(String usuario, String password);
	
	@Modifying
	@Query(value = "update usuarios u set u.id_carrito_actual = null where u.id_carrito_actual = :idCarrito", nativeQuery = true)
	void eliminarCarrito( @Param("idCarrito")  Long idCarrito);

	List<Usuario> findByVip(boolean b);

	@Query("Select u from Usuario u where u.vip = true AND u.fechaCambioVip > :inicio AND u.fechaCambioVip <:fin")
	List<Usuario> getExUsuariosNormalesByDate(LocalDateTime inicio, LocalDateTime fin);
	
	@Query("Select u from Usuario u where u.vip = false AND u.fechaCambioVip > :inicio AND u.fechaCambioVip <:fin")
	List<Usuario> getExUsuariosVipByDate(LocalDateTime inicio, LocalDateTime fin);

}
