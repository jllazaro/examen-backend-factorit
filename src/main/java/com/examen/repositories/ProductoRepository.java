package com.examen.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examen.entities.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

}
