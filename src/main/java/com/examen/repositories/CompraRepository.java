package com.examen.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen.entities.Compra;
import com.examen.entities.Usuario;

@Repository
public interface CompraRepository extends JpaRepository<Compra, Long> {

	@Query("Select c  from Compra c where c.usuario = :usuario AND (c.fechaCompra > :fechaCambioVip or :fechaCambioVip is null)")
	List<Compra> ultimasCompras(Usuario usuario,LocalDateTime fechaCambioVip);

}
