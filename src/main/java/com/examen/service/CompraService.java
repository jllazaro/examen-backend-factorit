package com.examen.service;

import java.util.List;

import com.examen.entities.Compra;
import com.examen.entities.Usuario;

public interface CompraService {

	List<Compra> ultimasCompras(Usuario usuario);

	Compra save(Compra compra);

}
