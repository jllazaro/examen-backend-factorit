package com.examen.service;

import java.time.LocalDate;
import java.util.List;

import com.examen.entities.Usuario;

public interface UsuarioService {

	Usuario findByUsuarioAndPassword(String usuario, String password);

	Usuario save(Usuario usuario);

	Usuario findById(Long idUsuario);

	List<Usuario> findAll();

	Usuario actualizarVip(Usuario usuario);

	void eliminarCarrito(Long id);

	List<Usuario> getUsuariosVip();

	List<Usuario> getExUsuariosNormalesByDate(LocalDate fecha);

	List<Usuario> getExUsuariosVipByDate(LocalDate fecha);

}
