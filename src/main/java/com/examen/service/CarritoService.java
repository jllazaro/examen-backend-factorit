package com.examen.service;

import com.examen.entities.Articulo;
import com.examen.entities.Carrito;
import com.examen.entities.Usuario;

public interface CarritoService {

	Carrito findById(Long id);

	Carrito agregarArticuloAlCarrito(Long idUsuario, Articulo articulo);

	void eliminarCarritosSinFinalizar();

	Carrito getCarrito(Long idUsuario);

	Carrito actualizarArticulo(Long idCarrito, Articulo articulo);

	Carrito eliminarArticuloCarrito(Long idCarrito, Articulo articulo);

	Carrito eliminarCarrito(Long idCarrito);

	Carrito finalizarCompra(Long idCarrito, Usuario usuario);

}
