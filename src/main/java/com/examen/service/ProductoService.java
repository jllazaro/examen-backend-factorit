package com.examen.service;

import java.util.List;

import com.examen.entities.Producto;

public interface ProductoService {

	List<Producto> findAll();

	Producto save(Producto producto);

}
