package com.examen.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.entities.Articulo;
import com.examen.entities.Carrito;
import com.examen.entities.Usuario;
import com.examen.service.CarritoService;

@RestController
@RequestMapping("api/carrito")
public class CarritoController {

	@Autowired
	private CarritoService carritoService;

//	@GetMapping("/{id}")
//	public ResponseEntity<Carrito> getById(@PathVariable(name = "id") Long id) throws Exception {
//		Carrito carrito = carritoService.findById(id);
//		if (carrito == null) {
//			throw new Exception("No se encontro Carrito");
//		}
//		return ResponseEntity.ok(carrito);
//	}

	@PostMapping("/{idUsuario}")
	public ResponseEntity<Carrito> agregarCarritoUsuario(@PathVariable(name = "idUsuario") Long idUsuario, @RequestBody Articulo articulo) throws Exception {
		Carrito carrito = carritoService.agregarArticuloAlCarrito(idUsuario, articulo);
		return ResponseEntity.ok(carrito);
	}
	
	@GetMapping("/{idUsuario}")
	public ResponseEntity<Carrito> getCarrito(@PathVariable(name = "idUsuario") Long idUsuario) throws Exception {
		Carrito carrito = carritoService.getCarrito(idUsuario);
		return ResponseEntity.ok(carrito);
	}

	
	@PostMapping("/actualizarArticulo/{idCarrito}")
	public ResponseEntity<Carrito> actualizarArticulo(@PathVariable(name = "idCarrito") Long idCarrito, @RequestBody Articulo articulo) throws Exception {
		Carrito carrito = carritoService.actualizarArticulo(idCarrito, articulo);
		return ResponseEntity.ok(carrito);
	}
	
	@PostMapping("/eliminarArticuloCarrito/{idCarrito}")
	public ResponseEntity<Carrito> eliminarArticuloCarrito(@PathVariable(name = "idCarrito") Long idCarrito, @RequestBody Articulo articulo) throws Exception {
		Carrito carrito = carritoService.eliminarArticuloCarrito(idCarrito, articulo);
		return ResponseEntity.ok(carrito);
	}
	
	@DeleteMapping("/eliminarCarrito/{idCarrito}")
	public ResponseEntity<Carrito> eliminarCarrito(@PathVariable(name = "idCarrito") Long idCarrito) throws Exception {
		Carrito carrito = carritoService.eliminarCarrito(idCarrito);
		return ResponseEntity.ok(carrito);
	}
	
	@RequestMapping("/finalizarCompra/{idCarrito}")
	public ResponseEntity<Carrito> finalizarCompra(@PathVariable(name = "idCarrito") Long idCarrito,  @RequestBody Usuario usuario) throws Exception {
		Carrito carrito = carritoService.finalizarCompra(idCarrito, usuario);
		return ResponseEntity.ok(carrito);
	}
}
