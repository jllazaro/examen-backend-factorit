package com.examen.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.entities.Producto;
import com.examen.serviceImpl.ProductoServiceImpl;

@RestController
@RequestMapping("api/producto")
public class ProductoController {

	@Autowired
	private ProductoServiceImpl productoService;
	
	@GetMapping
	public ResponseEntity<List<Producto>> getAllProductos(){
		List<Producto> productos = productoService.findAll();
		return ResponseEntity.ok(productos);
	}
	
	@PostMapping
	public ResponseEntity<Producto> getAllProductos(@RequestBody Producto producto){
		Producto productoResponse = productoService.save(producto);
		return ResponseEntity.ok(productoResponse);
	}
	
}
