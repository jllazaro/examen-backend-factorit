package com.examen.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.entities.Usuario;
import com.examen.service.UsuarioService;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuararioService;

	@PostMapping("/login")
	public ResponseEntity<Usuario> login(@RequestBody Usuario usuario) throws Exception {
		Usuario usuarioResult = usuararioService.findByUsuarioAndPassword(usuario.getUsuario(),
				usuario.getPassword());
		if (usuarioResult == null) {
			throw new Exception("No se encontro Usuario");
		}
		return ResponseEntity.ok(usuarioResult);
	}

	@PostMapping
	public ResponseEntity<Usuario> create(@RequestBody Usuario usuario) {
		Usuario usuarioResult = usuararioService.save(usuario);
		return ResponseEntity.ok(usuarioResult);
	}

	@GetMapping("/getUsuariosVip")
	public ResponseEntity<List<Usuario>> getUsuariosVip() {
		List<Usuario> usuariosResult = usuararioService.getUsuariosVip();
		return ResponseEntity.ok(usuariosResult);
	}
	
	@PostMapping("/getExUsuariosNormalesByDate")
	public ResponseEntity<List<Usuario>> getExUsuariosNormalesByDate(@RequestBody LocalDate fecha) {
		List<Usuario> usuariosResult = usuararioService.getExUsuariosNormalesByDate(fecha);
		return ResponseEntity.ok(usuariosResult);
	}
	@PostMapping("/getExUsuariosVipByDate")
	public ResponseEntity<List<Usuario>> getExUsuariosVipByDate(@RequestBody LocalDate fecha) {
		List<Usuario> usuariosResult = usuararioService.getExUsuariosVipByDate(fecha);
		return ResponseEntity.ok(usuariosResult);
	}
}
