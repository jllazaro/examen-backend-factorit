package com.examen.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "CarritoVip")
public class CarritoVip extends Carrito {
	@Override
	public Double descuento() {
		return 500D + precioProductoMasBarato();
	}
}
