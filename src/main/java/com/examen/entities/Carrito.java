package com.examen.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;

@Entity
@Table(name = "carritos")
@DiscriminatorColumn(name = "tipo")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Carrito {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "tipo", insertable = false, updatable = false)
	private String tipo;

	@OneToMany(mappedBy = "carrito", cascade = CascadeType.ALL)
	private List<Articulo> articulos = new ArrayList<Articulo>();

	@Column(name = "fecha_creacion", updatable = false)
	private LocalDateTime fechaCreacion;

	public Carrito() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}

	public void agregarArticulo(Articulo articulo) {
		this.articulos.add(articulo);
		articulo.setCarrito(this);
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@JsonPropertyDescription("importeTotal")
	public Double getImporteTotal() {
		return articulos.isEmpty() ? 0 : this.importeConDescuento();
	}

	@JsonPropertyDescription("importeSinDescuento")
	public Double getImporteSinDescuento() {
		return articulos.stream().mapToDouble(articulo -> articulo.getImporte()).sum();
	}

	private Double importeConDescuento() {
		if (cantidadArticulos() == 4) {
			return getImporteSinDescuento() * 0.75;
		} else if (cantidadArticulos() > 10) {
			return getImporteSinDescuento() - this.descuento();
		}
		return getImporteSinDescuento();
	}

	public Double descuento() {
		return 0D;
	};

	private Integer cantidadArticulos() {
		return articulos.size();
	}

	public Double precioProductoMasBarato() {
		if (cantidadArticulos() > 0) {
			return articulos.stream().mapToDouble(articulo -> articulo.getProducto().getPrecio()).min().getAsDouble();
		}
		return 0D;
	}

}
