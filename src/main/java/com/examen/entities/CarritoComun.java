package com.examen.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Comun")
public class CarritoComun extends Carrito {

	@Override
	public Double descuento() {
		return 100D;
	}

}
