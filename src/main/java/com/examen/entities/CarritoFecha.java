package com.examen.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "FechaEspecial")
public class CarritoFecha extends Carrito{

	@Override
	public Double descuento() {
		return 300D;
	}

}
