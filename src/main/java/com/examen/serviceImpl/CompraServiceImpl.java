package com.examen.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.entities.Compra;
import com.examen.entities.Usuario;
import com.examen.repositories.CompraRepository;
import com.examen.service.CompraService;

@Service
public class CompraServiceImpl implements CompraService {

	@Autowired
	CompraRepository compraRepository;

	@Override
	public Compra save(Compra compra) {
		return compraRepository.save(compra);
	}

	@Override
	public List<Compra> ultimasCompras(Usuario usuario) {
		return compraRepository.ultimasCompras(usuario, usuario.getFechaCambioVip());
	}

}
