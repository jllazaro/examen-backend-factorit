package com.examen.serviceImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.entities.Compra;
import com.examen.entities.Usuario;
import com.examen.repositories.UsuarioRepository;
import com.examen.service.CompraService;
import com.examen.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	CompraService comprasService;

	@Override
	public Usuario findByUsuarioAndPassword(String usuario, String password) {
		return usuarioRepository.findByUsuarioAndPassword(usuario, password);
	}

	@Override
	public Usuario save(Usuario usuario) {
		if (usuario.getId() == null) {
			usuario.setVip(false);
		}
		return usuarioRepository.save(usuario);
	}

	@Override
	public Usuario findById(Long idUsuario) {
		return usuarioRepository.findById(idUsuario).orElse(null);
	}

	@Override
	public Usuario actualizarVip(Usuario usuario) {
		List<Compra> ultimasCompras = comprasService.ultimasCompras(usuario);
		if (usuario.getVip()) {
			if (ultimasCompras.isEmpty() && usuario.getFechaCambioVip().plusMonths(1).isBefore(LocalDateTime.now())) {
//				si no compro nada en el ultimo mes despues de hacerse vip
				usuario.setVip(false);
				usuario.setFechaCambioVip(LocalDateTime.now());
			} else if (!ultimasCompras.isEmpty() && ultimasCompras.stream().map(compra -> compra.getFechaCompra())
					.max(LocalDateTime::compareTo).get().plusMonths(1).isBefore(LocalDateTime.now())) {
//				si paso un mes de su ultima compra
				usuario.setVip(false);
				usuario.setFechaCambioVip(LocalDateTime.now());
			}
		} else if (!usuario.getVip() && !ultimasCompras.isEmpty()
				&& ultimasCompras.stream().mapToDouble(compra -> compra.getImporte()).sum() > 10000) {
//			si no es vip, y tiene una de sus ultimas compras por mas de 10000 se pasa a vip
			usuario.setVip(true);
			usuario.setFechaCambioVip(LocalDateTime.now());
		}

		return usuario;
	}

	@Override
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	@Transactional
	@Override
	public void eliminarCarrito(Long idCarrito) {
		usuarioRepository.eliminarCarrito(idCarrito);
	}

	@Override
	public List<Usuario> getUsuariosVip() {
		return usuarioRepository.findByVip(true);
	}

	@Override
	public List<Usuario> getExUsuariosNormalesByDate(LocalDate fecha ) {
		LocalDateTime inicio = fecha.atStartOfDay();
		LocalDateTime fin = fecha.atTime(23, 59).plusMonths(1).minusDays(1);
		return usuarioRepository.getExUsuariosNormalesByDate(inicio, fin);
	}

	@Override
	public List<Usuario> getExUsuariosVipByDate(LocalDate fecha ) {
		LocalDateTime inicio = fecha.atStartOfDay();
		LocalDateTime fin = fecha.atTime(23, 59).plusMonths(1).minusDays(1);
		return usuarioRepository.getExUsuariosVipByDate(inicio, fin);
	}
}
