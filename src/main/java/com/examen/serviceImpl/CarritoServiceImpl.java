package com.examen.serviceImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.examen.entities.Articulo;
import com.examen.entities.Carrito;
import com.examen.entities.CarritoComun;
import com.examen.entities.CarritoFecha;
import com.examen.entities.CarritoVip;
import com.examen.entities.Compra;
import com.examen.entities.Usuario;
import com.examen.repositories.ArticuloRepository;
import com.examen.repositories.CarritoRepository;
import com.examen.service.CarritoService;
import com.examen.service.UsuarioService;

@Service
public class CarritoServiceImpl implements CarritoService {

	private static final Logger logger = Logger.getLogger(CarritoServiceImpl.class.toString());

	@Autowired
	private CarritoRepository carritoRepository;

	@Autowired
	private ArticuloRepository articuloRepository;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private CompraServiceImpl comprasService;

	@Value("${fechaEspecial}")
	private String fechaEspecial;

	@Override
	public Carrito findById(Long id) {
		return carritoRepository.findById(id).orElse(null);
	}

	@Override
	public Carrito agregarArticuloAlCarrito(Long idUsuario, Articulo articulo) {
		Carrito carrito = getCarrito(idUsuario);
		carrito.agregarArticulo(articulo);
		return carritoRepository.save(carrito);
	}

	@Override
	public Carrito getCarrito(Long idUsuario) {
		Usuario usuario = usuarioService.findById(idUsuario);
		Carrito carrito = null;

		if (usuario.getVip()) {
			carrito = new CarritoVip();
		} else if (this.esFechaEspecial()) {
			carrito = new CarritoFecha();
		} else {
			carrito = new CarritoComun();
		}
		if (usuario.getCarritoActual() == null) {
			carrito.setFechaCreacion(LocalDateTime.now());
			usuario.setCarritoActual(carrito);
			usuario = usuarioService.save(usuario);
		}
		return usuario.getCarritoActual();
	}

	private boolean esFechaEspecial() {
		return LocalDate.now().equals(LocalDate.parse(fechaEspecial));
	}

	@Override
	@Transactional
	public void eliminarCarritosSinFinalizar() {
		List<Carrito> carritos = new ArrayList<Carrito>();
		carritos = carritoRepository.carritosParaEliminar(LocalDate.now().atStartOfDay());
		if (!carritos.isEmpty()) {
			carritos.stream().forEach(carrito -> {
				logger.info("&&&&&&&&&&&&&&&&&&&Se procederá a eliminar el Carrito con id :" + carrito.getId()
						+ "&&&&&&&&&&&&&&&&&&&&&&&");
				usuarioService.eliminarCarrito(carrito.getId());
				carritoRepository.deleteById(carrito.getId());
			});
		}
	}

	@Transactional
	@Override
	public Carrito actualizarArticulo(Long idCarrito, Articulo articulo) {
		articuloRepository.actualizarCantidad(articulo.getId(), articulo.getCantidad());
		return this.findById(idCarrito);
	}

	@Transactional
	@Override
	public Carrito eliminarArticuloCarrito(Long idCarrito, Articulo articulo) {
		articuloRepository.delete(articulo);
		return this.findById(idCarrito);
	}

	@Override
	@Transactional
	public Carrito eliminarCarrito(Long idCarrito) {
		usuarioService.eliminarCarrito(idCarrito);
		carritoRepository.deleteById(idCarrito);
		return null;
	}

	@Override
	public Carrito finalizarCompra(Long idCarrito, Usuario usuario) {
		Carrito carrito = this.findById(idCarrito);
		Compra compra = new Compra(carrito, usuario);
		usuario.agregarCompra(compra);
		compra.setFechaCompra(LocalDateTime.now());
		compra.setImporte(carrito.getImporteTotal());
		comprasService.save(compra);
		usuario.setCarritoActual(null);
		this.usuarioService.actualizarVip(usuario);
		usuarioService.save(usuario);
		return null;
	}

}
