package com.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ExamenFactorItApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenFactorItApplication.class, args);
	}

}
