package com.examen.batch;



import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.examen.entities.Usuario;
import com.examen.service.CarritoService;
import com.examen.service.UsuarioService;

@Component
public class TaskService {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private CarritoService carritoService;

	private static final Logger logger = Logger.getLogger(TaskService.class.toString());
	
//	@Scheduled(cron = "0 0 0 * * *")
	@Scheduled(fixedRate = 1000 * 30)
	public void actualizarUsuariosVip() {
		logger.info("===========================================================================================");
		logger.info("============================COMIENZO ACTUALIZAR USUARIOS VIP===============================");
		logger.info("===========================================================================================");
		
		usuarioService.findAll().stream().forEach(usuario ->{
			Boolean vipAntes = usuario.getVip();
			Usuario usuarioResult = usuarioService.actualizarVip(usuario);
			if(!vipAntes.equals(usuarioResult.getVip())) {
				logger.info("&&&&&&&&&&&&&&&&&& Se actualizo el usuario"+ usuarioResult.getUsuario() + (usuarioResult.getVip()? " AHORA ES VIP": " DEJÓ DE SER VIP") +"&&&&&&&&&&&&&&&&&&&&");
			}
			usuarioService.save(usuarioResult);
		});
		
		logger.info("===========================================================================================");
		logger.info("==============================FIN ACTUALIZAR USUARIOS VIP==================================");
		logger.info("===========================================================================================");
	}
	
//	@Scheduled(cron = "0 0 0 * * *")
	@Scheduled(fixedRate = 1000 * 30)
	public void eliminarCarritosSinFinalizar() {
		logger.info("===========================================================================================");
		logger.info("================================ COMIENZO ELIMINAR CARRITOS ==================================");
		logger.info("===========================================================================================");
		
		carritoService.eliminarCarritosSinFinalizar();
		logger.info("===========================================================================================");
		logger.info("================================ FIN ELIMINAR CARRITOS ======================================");
		logger.info("===========================================================================================");
	}
	
}
