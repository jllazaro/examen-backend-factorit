# Examen Factor IT - BackEnd

[![Build Status](https://travis-ci.org/hmcts/spring-boot-template.svg?branch=master)](https://travis-ci.org/hmcts/spring-boot-template)

## Información del Proyecto

Este proyecto se desarrollo en JavaSE-11, usando Spring Boot.

## Pasos para Deploy

Seguir los siguentes pasos para poder correr la aplicación correctamente:

 * Abra alguna consola de Git y dirigase a la ubicación donde realizará la descarga del proyecto
 
 * Escriba el comando: git clone https://gitlab.com/jllazaro/examen-backend-factorit.git
 
 * Abra IDE Eclipse e importe el proyecto como proyecto Maven
 
 * Aguarde que installen las dependencias
 
 * Verifique que los valores del archivo application.properties (ubicado en src/main/resources) contiene los valores correctos
    para la conexión a su base de datos MYSQ:
        spring.datasource.url=jdbc:mysql://localhost:3306/examen
        spring.datasource.username=root
        spring.datasource.password=root
 
 * Ya sea por consola o en una hoja SQL ejecute la query: create schema examen;

 * Ahora si podemos arrancar el proyecto haciendo click derecho Run As>>Spring Boot App. Hibernate creara las tablas automaticamente.
 
 * El archivo TemplateInsertInicialMySQL.xlsx contiene una plantilla para poder generar querys Mysql para el 
    insert en Produtos y Usuarios, si desea puede usarla, de lo contrario corra estos scripts SQL:

        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy S7 32 GB negro 4 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_861658-MLA31933448747_082019-V.webp', 6900);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy A31 128 GB prism crush black 4 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_612143-MLA42956941562_072020-V.webp', 359.9);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy A21s 64 GB negro 4 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_848465-MLA43441025669_092020-V.webp', 2099.9);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy J5 Prime 16 GB negro 2 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_780098-MLA31003118674_062019-V.webp', 2200);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy S10 128 GB negro prisma 8 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_914989-MLA31578002339_072019-V.webp', 10000);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('Samsung Galaxy A51 128 GB prism crush blue 4 GB RAM', 'https://http2.mlstatic.com/D_NQ_NP_903730-MLA41240273524_032020-V.webp', 10000.01);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('iPhone X 64 GB plata', 'https://http2.mlstatic.com/D_NQ_NP_635226-MLA31003000949_062019-V.webp', 16999);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('iPhone 11 Pro Max 64 GB Plata', 'https://http2.mlstatic.com/D_NQ_NP_620546-MLA32445355354_102019-V.webp', 24999);
        INSERT INTO examen.productos (descripcion, imagen, precio) values ('iPhone 6s Plus 16 GB oro rosa', 'https://http2.mlstatic.com/D_NQ_NP_976473-MLA31003091084_062019-V.webp', 9999.9);


        INSERT INTO examen.usuarios (usuario, password) values ('jllazaro', '123');
        INSERT INTO examen.usuarios (usuario, password) values ('ipavon', '123');
        INSERT INTO examen.usuarios (usuario, password) values ('ysoto', '123');
        INSERT INTO examen.usuarios (usuario, password) values ('esalcedo', '123');

 * Para uno de los requerimiento se necesito tener un parametro para la fecha, el mismo se encuentra en application.properties:

    fechaEspecial = 2020-11-12

 * Por último para la eliminación del carrito "Abandonado", es decir que no culminó su compra se corre un proceso automático, al   igual que para la 
    actualización de los usuarios VIP. en ambos casos puede editarse la frecuencia en que se ejecuta el proceso:
    
    @Scheduled(fixedRate = milisegundos)

